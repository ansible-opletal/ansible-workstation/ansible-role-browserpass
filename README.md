ansible-role-browserpass
=========

This role prepared a workstation for the browserpass browser extension, e.g. [this](https://addons.mozilla.org/en-US/firefox/addon/browserpass-ce/)

Role Variables
--------------

This role inherits the variables from [ansible-role-packages](https://gitlab.com/ansible-opletal/ansible-workstation/ansible-role-packages), from where it
decides for which browser to install the native browserpass app

License
-------

BSD
